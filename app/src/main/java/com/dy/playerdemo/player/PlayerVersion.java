package com.dy.playerdemo.player;

/**
 * @ClassName: PlayerVersion(com.dy.playerdemo.player)
 * @Create: 2019/12/19 19:06 by panchao
 * @Description:
 */
public enum PlayerVersion {
    VERSION1,//video的pts 与 audioTrack.getPlaybackHeadPosition()返回的播放时间进行对比
    VERSION2,//考虑AudioTrack-->getLatency的内部延迟，不推荐使用
    VERSION3,//使用AudioTrack-->getTimestamp方法(API 19以上支持)
    VERSION4,//基于vsync调整Releasetime,并且使用带时间戳的releaseOutputBuffer接口(API 21以上版本使用)
    VERSION5 //提前两倍vsync duration时间调用releaseOutputBuffer接口
}
