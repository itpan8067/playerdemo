package com.dy.playerdemo.player;

/**
 * @ClassName: IMediaTimeProvider(com.dy.playerdemo.player)
 * @Create: 2019/12/19 16:25 by panchao
 * @Description:
 */
public interface IMediaTimeProvider {
    long getNowUs();
    long getRealTimeUsForMediaTime(long mediaTimeUs);
    long getVsyncDurationNs();
}
