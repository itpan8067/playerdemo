package com.dy.playerdemo.player;

import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.net.Uri;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: MediaCodecPlayer(com.dy.playerdemo.player)
 * @Create: 2019/12/19 16:07 by panchao
 * @Description:
 */
public class MediaCodecPlayer implements IMediaTimeProvider{

    private static final String TAG = "MediaCodecPlayer";
    private SurfaceHolder mSurfaceHolder;
    private Context mContext;

    private Uri mAudioUri;
    private Map<String, String> mAudioHeaders;
    private Uri mVideoUri;
    private Map<String, String> mVideoHeaders;

    private MediaExtractor mAudioExtractor;
    private MediaExtractor mVideoExtractor;
    private Map<Integer, AudioCodecState> mAudioCodecStates;
    private Map<Integer, VideoCodecState> mVideoCodecStates;
    private AudioCodecState mAudioTrackState;
    private long mDurationUs;
    private long mDeltaTimeUs;

    private static final int STATE_IDLE = 1;
    private static final int STATE_PREPARING = 2;
    private static final int STATE_PLAYING = 3;
    private static final int STATE_PAUSED = 4;
    private Integer mState;

    private Thread mThread;
    private Boolean mThreadStarted = false;

    private VideoFrameReleaseTimeHelper mFrameReleaseTimeHelper;

    private PlayerVersion mVersion;

    public MediaCodecPlayer(SurfaceHolder surfaceHolder, Context context, PlayerVersion version) {
        mSurfaceHolder = surfaceHolder;
        mContext = context;
        mVersion = version;

        mFrameReleaseTimeHelper = new VideoFrameReleaseTimeHelper(context);

        mState = STATE_IDLE;
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (mThreadStarted) {
                        if (mThreadStarted == false) {
                            break;
                        }
                    }
                    synchronized (mState) {
                        if (mState == STATE_PLAYING) {
                            doSomeWork();
                            if (mAudioTrackState != null) {
                                mAudioTrackState.process();
                            }
                        }
                    }
                    try {
                        Thread.sleep(5);//5ms loop
                    } catch (InterruptedException ex) {
                        Log.d(TAG, "Thread interrupted");
                    }
                }
            }
        });
    }

    public void setAudioDataSource(Uri uri, Map<String, String> headers) {
        mAudioUri = uri;
        mAudioHeaders = headers;
    }

    public void setVideoDataSource(Uri uri, Map<String, String> headers) {
        mVideoUri = uri;
        mVideoHeaders = headers;
    }

    public boolean prepare() {
        mState = STATE_PREPARING;
        if (null == mAudioExtractor) {
            mAudioExtractor = new MediaExtractor();
            if (null == mAudioExtractor) {
                Log.e(TAG, "prepare - Cannot create Audio extractor.");
                mState = STATE_IDLE;
                return false;
            }
        }

        if (null == mVideoExtractor){
            mVideoExtractor = new MediaExtractor();
            if (null == mVideoExtractor) {
                Log.e(TAG, "prepare - Cannot create Video extractor.");
                mState = STATE_IDLE;
                return false;
            }
        }

        try {
            mAudioExtractor.setDataSource(mAudioUri.toString(), mAudioHeaders);
            mVideoExtractor.setDataSource(mVideoUri.toString(), mVideoHeaders);
            if (null == mVideoCodecStates) {
                mVideoCodecStates = new HashMap<Integer, VideoCodecState>();
            } else {
                mVideoCodecStates.clear();
            }

            if (null == mAudioCodecStates) {
                mAudioCodecStates = new HashMap<Integer, AudioCodecState>();
            } else {
                mAudioCodecStates.clear();
            }
            if (!prepareAudio()) {
                Log.e(TAG,"prepare - prepareAudio() failed!");
                mState = STATE_IDLE;
                return false;
            }
            if (!prepareVideo()) {
                Log.e(TAG,"prepare - prepareVideo() failed!");
                mState = STATE_IDLE;
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        synchronized (mState) {
            mState = STATE_PAUSED;
        }
        return true;
    }

    public void start() {
        mFrameReleaseTimeHelper.enable();

        synchronized (mState) {
            if (mState == STATE_IDLE ) {
                return;
            } else if (mState == STATE_PLAYING || mState == STATE_PREPARING) {
                return;
            } else if (mState != STATE_PAUSED) {
                throw new IllegalStateException();
            }

            for (VideoCodecState state : mVideoCodecStates.values()) {
                state.start();
            }

            for (AudioCodecState state : mAudioCodecStates.values()) {
                state.start();
            }
            mDeltaTimeUs = -1;
            mState = STATE_PLAYING;
        }

        synchronized (mThreadStarted) {
            mThreadStarted = true;
            mThread.start();
        }
    }

    public void pause() {
        Log.d(TAG, "pause");

        synchronized (mState) {
            if (mState == STATE_PAUSED) {
                return;
            } else if (mState != STATE_PLAYING) {
                throw new IllegalStateException();
            }

            for (VideoCodecState state : mVideoCodecStates.values()) {
                state.pause();
            }

            for (AudioCodecState state : mAudioCodecStates.values()) {
                state.pause();
            }
            mState = STATE_PAUSED;
        }
    }

    public void release() {
        synchronized (mState) {
            if (mState == STATE_PLAYING) {
                pause();
            }
            if (mVideoCodecStates != null) {
                for (VideoCodecState state : mVideoCodecStates.values()) {
                    state.release();
                }
                mVideoCodecStates = null;
            }

            if (mAudioCodecStates != null) {
                for (AudioCodecState state : mAudioCodecStates.values()) {
                    state.release();
                }
                mAudioCodecStates = null;
            }

            if (mAudioExtractor != null) {
                mAudioExtractor.release();
                mAudioExtractor = null;
            }

            if (mVideoExtractor != null) {
                mVideoExtractor.release();
                mVideoExtractor = null;
            }

            if (mFrameReleaseTimeHelper != null) {
                mFrameReleaseTimeHelper.disable();
                mFrameReleaseTimeHelper = null;
            }

            mDurationUs = -1;
            mState = STATE_IDLE;
        }
        synchronized (mThreadStarted) {
            mThreadStarted = false;
        }
        try {
            mThread.join();
        } catch (InterruptedException ex) {
            Log.d(TAG, "mThread.join " + ex);
        }
    }

    private void doSomeWork() {
        try {
            for (VideoCodecState state : mVideoCodecStates.values()) {
                state.doSomeWork();
            }
        } catch (IllegalStateException e) {
            throw new Error("Video CodecState.doSomeWork" + e);
        }

        try {
            for (AudioCodecState state : mAudioCodecStates.values()) {
                state.doSomeWork();
            }
        } catch (IllegalStateException e) {
            throw new Error("Audio CodecState.doSomeWork" + e);
        }
    }

    @Override
    public long getNowUs() {
        if (mAudioTrackState == null) {//如果是video only的流，则返回系统时间，否则返回audio播放的时间
            return System.currentTimeMillis() * 1000;
        }
        return mAudioTrackState.getAudioTimeUs();
    }

    @Override
    public long getRealTimeUsForMediaTime(long mediaTimeUs) {
        if (mVersion == PlayerVersion.VERSION1 || mVersion == PlayerVersion.VERSION2 || mVersion == PlayerVersion.VERSION3) {
            if (mDeltaTimeUs == -1) {//第一次调用的时候会走进这个分支，mDeltaTimeUs代表初始pts的偏差，正常情况下它是0
                long nowUs = getNowUs();
                mDeltaTimeUs = nowUs - mediaTimeUs;
            }
            return mDeltaTimeUs + mediaTimeUs;//所以getRealTimeUsForMediaTime返回的就是pts
        }else {
            if (mDeltaTimeUs == -1) {
                long nowUs = getNowUs();
                mDeltaTimeUs = nowUs - mediaTimeUs;
            }
            long earlyUs = mDeltaTimeUs + mediaTimeUs - getNowUs();//video的pts - audio的pts
            long unadjustedFrameReleaseTimeNs = System.nanoTime() + (earlyUs * 1000);//然后用当前时间(ns) + 差值
            //通过video的pts时间、未校正的系统时间、vsync三者一起进行校正操作
            long adjustedReleaseTimeNs = mFrameReleaseTimeHelper.adjustReleaseTime(mDeltaTimeUs + mediaTimeUs, unadjustedFrameReleaseTimeNs);
            return adjustedReleaseTimeNs / 1000;
        }
    }

    @Override
    public long getVsyncDurationNs() {
        if (mVersion == PlayerVersion.VERSION1 || mVersion == PlayerVersion.VERSION2 || mVersion == PlayerVersion.VERSION3) {
            return 0;
        }else {
            if (mFrameReleaseTimeHelper != null) {
                long time = mFrameReleaseTimeHelper.getVsyncDurationNs();
                return time;
            } else {
                return -1;
            }
        }
    }

    private boolean prepareAudio() throws IOException {
        for (int i = mAudioExtractor.getTrackCount(); i-- > 0;) {
            MediaFormat format = mAudioExtractor.getTrackFormat(i);
            String mime = format.getString(MediaFormat.KEY_MIME);

            if (!mime.startsWith("audio/")) {
                continue;
            }

            Log.d(TAG, "audio track #" + i + " " + format + " " + mime +
                    " Is ADTS:" + getMediaFormatInteger(format, MediaFormat.KEY_IS_ADTS) +
                    " Sample rate:" + getMediaFormatInteger(format, MediaFormat.KEY_SAMPLE_RATE) +
                    " Channel count:" +
                    getMediaFormatInteger(format, MediaFormat.KEY_CHANNEL_COUNT));

            mAudioExtractor.selectTrack(i);
            if (!addTrack(i, format)) {
                Log.e(TAG, "prepareAudio - addTrack() failed!");
                return false;
            }

            if (format.containsKey(MediaFormat.KEY_DURATION)) {
                long durationUs = format.getLong(MediaFormat.KEY_DURATION);

                if (durationUs > mDurationUs) {
                    mDurationUs = durationUs;
                }
                Log.d(TAG, "audio track format #" + i +
                        " Duration:" + mDurationUs + " microseconds");
            }
        }
        return true;
    }

    private boolean prepareVideo() throws IOException {
        for (int i = mVideoExtractor.getTrackCount(); i-- > 0;) {
            MediaFormat format = mVideoExtractor.getTrackFormat(i);
            String mime = format.getString(MediaFormat.KEY_MIME);

            if (!mime.startsWith("video")) {
                continue;
            }

            int height = getMediaFormatInteger(format, MediaFormat.KEY_HEIGHT);
            int width = getMediaFormatInteger(format, MediaFormat.KEY_WIDTH);
            Log.d(TAG, "video track #" + i + " " + format + " " + mime +
                    " Width:" + width + ", Height:" + height);

            mVideoExtractor.selectTrack(i);
            if (!addTrack(i, format)) {
                Log.e(TAG, "prepareVideo - addTrack() failed!");
                return false;
            }

            if (format.containsKey(MediaFormat.KEY_DURATION)) {
                long durationUs = format.getLong(MediaFormat.KEY_DURATION);

                if (durationUs > mDurationUs) {
                    mDurationUs = durationUs;
                }
                Log.d(TAG, "track format #" + i + " Duration:" + mDurationUs + " microseconds");
            }
        }
        return true;
    }

    private boolean addTrack(int trackIndex, MediaFormat format) throws IOException {
        String mime = format.getString(MediaFormat.KEY_MIME);
        boolean isVideo = mime.startsWith("video/");
        boolean isAudio = mime.startsWith("audio/");
        MediaCodec codec;

        codec = MediaCodec.createDecoderByType(mime);
        if (codec == null) {
            Log.e(TAG, "addTrack - Could not create regular playback codec for mime "+
                    mime+"!");
            return false;
        }
        codec.configure(
                format,
                isVideo ? mSurfaceHolder.getSurface() : null, null, 0);

        if (isVideo) {
            VideoCodecState state = new VideoCodecState(this, mVideoExtractor, trackIndex, format, codec, mVersion);
            mVideoCodecStates.put(Integer.valueOf(trackIndex), state);
        } else {
            AudioCodecState state = new AudioCodecState(this, mAudioExtractor, trackIndex, format, codec, true, mVersion);
            mAudioCodecStates.put(Integer.valueOf(trackIndex), state);
            mAudioTrackState = state;
        }

        return true;
    }

    protected int getMediaFormatInteger(MediaFormat format, String key) {
        return format.containsKey(key) ? format.getInteger(key) : 0;
    }
}
