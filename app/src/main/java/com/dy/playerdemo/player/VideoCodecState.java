package com.dy.playerdemo.player;

import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;

import com.dy.playerdemo.player.IMediaTimeProvider;

import java.nio.ByteBuffer;
import java.util.LinkedList;

import androidx.annotation.RequiresApi;

/**
 * Class for directly managing both audio and video playback by
 * using {@link MediaCodec} and {@link AudioTrack}.
 */
public class VideoCodecState {
    private static final String TAG = VideoCodecState.class.getSimpleName();

    private boolean mSawInputEOS, mSawOutputEOS;
    private ByteBuffer[] mCodecInputBuffers;
    private ByteBuffer[] mCodecOutputBuffers;
    private int mTrackIndex;
    private LinkedList<Integer> mAvailableInputBufferIndices;
    private LinkedList<Integer> mAvailableOutputBufferIndices;
    private LinkedList<MediaCodec.BufferInfo> mAvailableOutputBufferInfos;
    private long mPresentationTimeUs;
    private long mSampleBaseTimeUs;
    private MediaCodec mCodec;
    private IMediaTimeProvider mMediaTimeProvider;
    private MediaExtractor mExtractor;
    private MediaFormat mFormat;
    private MediaFormat mOutputFormat;
    private PlayerVersion mVersion;

    /**
     * Manages audio and video playback using MediaCodec and AudioTrack.
     */
    public VideoCodecState (IMediaTimeProvider mediaTimeProvider,
                            MediaExtractor extractor,
                            int trackIndex,
                            MediaFormat format,
                            MediaCodec codec,
                            PlayerVersion version) {
        mMediaTimeProvider = mediaTimeProvider;
        mExtractor = extractor;
        mTrackIndex = trackIndex;
        mFormat = format;

        mCodec = codec;

        mAvailableInputBufferIndices = new LinkedList<Integer>();
        mAvailableOutputBufferIndices = new LinkedList<Integer>();
        mAvailableOutputBufferInfos = new LinkedList<MediaCodec.BufferInfo>();

        mSawInputEOS = mSawOutputEOS = false;
        mSampleBaseTimeUs = -1;
        mPresentationTimeUs = 0;

        mVersion = version;
    }

    public void release() {
        mCodec.stop();
        mCodecInputBuffers = null;
        mCodecOutputBuffers = null;
        mOutputFormat = null;

        mAvailableInputBufferIndices.clear();
        mAvailableOutputBufferIndices.clear();
        mAvailableOutputBufferInfos.clear();

        mAvailableInputBufferIndices = null;
        mAvailableOutputBufferIndices = null;
        mAvailableOutputBufferInfos = null;

        mCodec.release();
        mCodec = null;
    }

    public void start() {
        mCodec.start();
        mCodecInputBuffers = mCodec.getInputBuffers();
        mCodecOutputBuffers = mCodec.getOutputBuffers();
    }

    public void pause() {
        //undo
    }

    public long getCurrentPositionUs() {
        return mPresentationTimeUs;
    }

    public void flush() {
        mAvailableInputBufferIndices.clear();
        mAvailableOutputBufferIndices.clear();
        mAvailableOutputBufferInfos.clear();

        mSawInputEOS = false;
        mSawOutputEOS = false;

        mCodec.flush();
    }

    public boolean isEnded() {
        return mSawInputEOS && mSawOutputEOS;
    }

    /**
     * doSomeWork() is the worker function that does all buffer handling and decoding works.
     * It first reads data from {@link MediaExtractor} and pushes it into {@link MediaCodec};
     * it then dequeues buffer from {@link MediaCodec}, consumes it and pushes back to its own
     * buffer queue for next round reading data from {@link MediaExtractor}.
     */
    public void doSomeWork() {
        int indexInput = mCodec.dequeueInputBuffer(0 /* timeoutUs */);
        if (indexInput != MediaCodec.INFO_TRY_AGAIN_LATER) {
            mAvailableInputBufferIndices.add(indexInput);
        }
        while (feedInputBuffer()) {
        }

        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        int indexOutput = mCodec.dequeueOutputBuffer(info, 0 /* timeoutUs */);

        if (indexOutput == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
            mOutputFormat = mCodec.getOutputFormat();
            onOutputFormatChanged();
        } else if (indexOutput == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
            mCodecOutputBuffers = mCodec.getOutputBuffers();
        } else if (indexOutput != MediaCodec.INFO_TRY_AGAIN_LATER) {
            mAvailableOutputBufferIndices.add(indexOutput);
            mAvailableOutputBufferInfos.add(info);
        }

        while (drainOutputBuffer()) {
        }
    }

    /** Returns true if more input data could be fed. */
    private boolean feedInputBuffer() throws MediaCodec.CryptoException, IllegalStateException {
        if (mSawInputEOS || mAvailableInputBufferIndices.isEmpty()) {
            return false;
        }

        int index = mAvailableInputBufferIndices.peekFirst().intValue();
        ByteBuffer codecData = mCodecInputBuffers[index];
        int trackIndex = mExtractor.getSampleTrackIndex();
        if (trackIndex == mTrackIndex) {
            int sampleSize = mExtractor.readSampleData(codecData, 0 /* offset */);
            long sampleTime = mExtractor.getSampleTime();
            int sampleFlags = mExtractor.getSampleFlags();

            if (sampleSize <= 0) {
                Log.d(TAG, "sampleSize: " + sampleSize + " trackIndex:" + trackIndex + " sampleTime:" + sampleTime + " sampleFlags:" + sampleFlags);
                mSawInputEOS = true;
                return false;
            }

            if (mSampleBaseTimeUs == -1) {
                mSampleBaseTimeUs = sampleTime;
            }
            sampleTime -= mSampleBaseTimeUs;
            // this is just used for getCurrentPosition, not used for avsync
            mPresentationTimeUs = sampleTime;

            if ((sampleFlags & MediaExtractor.SAMPLE_FLAG_ENCRYPTED) != 0) {
                MediaCodec.CryptoInfo info = new MediaCodec.CryptoInfo();
                mExtractor.getSampleCryptoInfo(info);
                mCodec.queueSecureInputBuffer(index, 0 /* offset */, info, sampleTime, 0 /* flags */);
            } else {
                mCodec.queueInputBuffer(index, 0 /* offset */, sampleSize, sampleTime, 0 /* flags */);
            }

            mAvailableInputBufferIndices.removeFirst();
            mExtractor.advance();

            return true;
        } else if (trackIndex < 0) {
            Log.d(TAG, "saw input EOS on track " + mTrackIndex);
            mSawInputEOS = true;
            mCodec.queueInputBuffer(index, 0 /* offset */, 0 /* sampleSize */, 0 /* sampleTime */, MediaCodec.BUFFER_FLAG_END_OF_STREAM);

            mAvailableInputBufferIndices.removeFirst();
        }

        return false;
    }

    private void onOutputFormatChanged() {
        String mime = mOutputFormat.getString(MediaFormat.KEY_MIME);
        Log.d(TAG, "CodecState::onOutputFormatChanged " + mime);
        if (mime.startsWith("video/")) {
            int width = mOutputFormat.getInteger(MediaFormat.KEY_WIDTH);
            int height = mOutputFormat.getInteger(MediaFormat.KEY_HEIGHT);
            Log.d(TAG, "CodecState::onOutputFormatChanged Video" + " width:" + width + " height:" + height);
        }
    }

    /** Returns true if more output data could be drained. */
    //audio and video belongs to different codecstate, one has audio, the other one dont
    //so there exists two mPresentationTimeUs, one for audio, the other one for video
    //however, audio and video draining works in the same thread
    private boolean drainOutputBuffer() {
        if (mSawOutputEOS || mAvailableOutputBufferIndices.isEmpty()) {
            return false;
        }

        int index = mAvailableOutputBufferIndices.peekFirst().intValue();
        MediaCodec.BufferInfo info = mAvailableOutputBufferInfos.peekFirst();

        if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
            Log.d(TAG, "saw output EOS on track " + mTrackIndex);

            mSawOutputEOS = true;

            return false;
        }

        boolean render;

        long realTimeUs = mMediaTimeProvider.getRealTimeUsForMediaTime(info.presentationTimeUs); //映射到nowUs时间轴上
        long nowUs = mMediaTimeProvider.getNowUs(); //audio play time

        if (mVersion == PlayerVersion.VERSION1 || mVersion == PlayerVersion.VERSION2 || mVersion == PlayerVersion.VERSION3) {
            long lateUs = nowUs  - realTimeUs;
            if (lateUs < -45000) {
                // too early;如果video来早了45ms，则等待下一次loop
                return false;
            } else if (lateUs > 30000) {
                //默认的丢帧门限是30ms
                render = false;
            } else {
                render = true;
                mPresentationTimeUs = info.presentationTimeUs;
            }
            mCodec.releaseOutputBuffer(index, render);
        }else if (mVersion == PlayerVersion.VERSION4) {
            long lateUs = System.nanoTime()/1000 - realTimeUs;
            if (lateUs < -45000) {
                // too early;如果video来早了45ms，则等待下一次loop
                return false;
            } else if (lateUs > 30000) {
                //默认的丢帧门限是30ms
                render = false;
            } else {
                render = true;
                mPresentationTimeUs = info.presentationTimeUs;
            }
            mCodec.releaseOutputBuffer(index, realTimeUs*1000);
        }else {
            long twiceVsyncDurationUs = 2 * mMediaTimeProvider.getVsyncDurationNs()/1000;
            long lateUs = System.nanoTime()/1000 - realTimeUs;
            if (lateUs < -twiceVsyncDurationUs) {
                Log.e("avsync", "@@早了@@ lateUs=" + lateUs + ", realTimeUs=" + realTimeUs + ", pts=" + info.presentationTimeUs + ", nowUs=" + nowUs + ", vsync=" + mMediaTimeProvider.getVsyncDurationNs() + ", twicevsync=" + twiceVsyncDurationUs);
                return false;
            } else if (lateUs > 30000) {
                Log.e("avsync", "@@晚了@@ lateUs=" + lateUs + ", realTimeUs=" + realTimeUs + ", pts=" + info.presentationTimeUs + ", nowUs=" + nowUs + ", vsync=" + mMediaTimeProvider.getVsyncDurationNs() + ", twicevsync=" + twiceVsyncDurationUs);
                render = false;
            } else {
                Log.e("avsync", "@@刚好@@ lateUs=" + lateUs + ", realTimeUs=" + realTimeUs + ", pts=" + info.presentationTimeUs + ", nowUs=" + nowUs + ", vsync=" + mMediaTimeProvider.getVsyncDurationNs() + ", twicevsync=" + twiceVsyncDurationUs);
                render = true;
                mPresentationTimeUs = info.presentationTimeUs;
            }
            mCodec.releaseOutputBuffer(index, realTimeUs*1000);
        }

        mAvailableOutputBufferIndices.removeFirst();
        mAvailableOutputBufferInfos.removeFirst();
        return true;
    }
}
